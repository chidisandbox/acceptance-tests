package fr.chidix;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.thoughtworks.gauge.ClassInitializer;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
@ComponentScan
public class SpringRegistrator implements ClassInitializer {

	private ApplicationContext applicationContext;

	@Override
	public Object initialize(Class<?> classToInitialize) throws Exception {
		log.debug("Application context: " + applicationContext);
		log.debug("Class to initialize: " + classToInitialize);
		if (applicationContext == null) {
			log.debug("Initializing Spring context...");
			applicationContext = new AnnotationConfigApplicationContext(SpringRegistrator.class);
		}
		String[] beanNames = applicationContext.getBeanNamesForType(classToInitialize);
		if (beanNames.length == 0) {
			throw new NoSuchBeanDefinitionException(classToInitialize.getName());
		}
		String s = beanNames[0];
		return classToInitialize.cast(applicationContext.getBean(s));
	}

}
